using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Powerups/SpeedBuff")]
public class speedBuff : PowerBuff
{
    public float amount;
    public override void Apply(GameObject target)
    {
        target.GetComponent<Movement>().Speed += amount;
        target.GetComponent<shooting>().bulletforce +=10f ;
        target.GetComponent<SpriteRenderer>().color = Color.yellow;
    }
}
