using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthDisplay : MonoBehaviour
{
    public int health;
    public int maxHealth;

    public Sprite blankhp;
    public Sprite fullhp;
    public Image[] hearts;

    public PlayerHealth playerhealth;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        health = playerhealth.health;
        maxHealth = playerhealth.maxHealth;
        for (int i = 0; i < hearts.Length; i++)
        {
            if (i < health)
            {
                hearts[i].sprite = fullhp;
            }
            else
            {
                hearts[i].sprite = blankhp;
            }
            if (i < maxHealth)
            {
                hearts[i].enabled = true;
            }
        else
            {
                hearts[i].enabled = false;
            }
        }
    }
}
