using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int health;
    public int MaxHealth;

    // Start is called before the first frame update
    void Start()
    {
        health = MaxHealth;
    }
    void Update()
    {
        if (health <= 0)
        {
            Destroy(gameObject);
        }
    }
    // Update is called once per frame
    public void TakeDamage(int damage)
    {
        health -= damage;
    }
}
