using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    public PowerBuff powerbuff;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(gameObject);
        powerbuff.Apply(collision.gameObject);
    }
}
