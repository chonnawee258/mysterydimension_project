using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerHealth : MonoBehaviour
{
    public static event Action OnPlayerDeath;
    public int health;
    public int maxHealth = 10;

    public SpriteRenderer sr;
    public Movement Move;
    // Start is called before the first frame update
    void Start()
    {
        health = maxHealth;
    }

    // Update is called once per frame
    public void TakeDamage(int amount)
    {
        health -= amount;
        if(health <= 0)
        {
            sr.enabled = false;
            Move.enabled = false;

            health = 0;
            Debug.Log("you dead");
            OnPlayerDeath?.Invoke();


            Destroy(gameObject);

        }
    }
}
