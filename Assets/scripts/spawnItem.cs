using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnItem : MonoBehaviour
{

    public GameObject[] buffPrefabs;



    private float spawnLimitXLeft = -22;

    private float spawnLimitXRight = 7;

    private float spawnPosY = 30;



    private float startDelay = 1;

    private float spawnInterval;



    void Start()

    {

        Invoke("SpawnRandomBuff", startDelay);

    }



    void SpawnRandomBuff()

    {

        Vector3 spawnPos = new Vector3(Random.Range(spawnLimitXLeft, spawnLimitXRight), spawnPosY, 0);

        int randomBuff = Random.Range(0, 3);



        Instantiate(buffPrefabs[randomBuff], spawnPos, buffPrefabs[randomBuff].transform.rotation);



        spawnInterval = Random.Range(3, 5);

        Invoke("SpawnRandomBuff", spawnInterval);

    }



}
