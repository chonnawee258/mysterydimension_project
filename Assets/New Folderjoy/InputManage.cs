using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System;

public class InputManage : MonoBehaviour
{
    public Vector2 MoveInput;
    public bool isAttackbtnHold;
    public void OnMove(InputValue input) => MoveInput = input.Get<Vector2>();

    public void OnAttack()
    {
        GetComponent<Attack>().DoSomeAttack();
    }

    public void OnAttackHold(InputValue input)
    {
        isAttackbtnHold = Convert.ToBoolean(input.Get<float>());
    }
}
