using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    public GameObject bulletprefabs;
    public Transform firepoint;

    public void  DoSomeAttack()
    {
        GameObject clone = Instantiate(bulletprefabs, firepoint.position, Quaternion.identity);

        Rigidbody2D rb = clone.GetComponent<Rigidbody2D>();
        rb.AddForce(firepoint.up * 50, ForceMode2D.Impulse);
    }

}
